#include <stdlib.h>
#include <stdio.h>
#include <argtable3.h>

//TODO PCFX special comments
//TODO option to autodetect invalid data 
//TODO show data LE or BE

const char *const v810_registers[]=
{
"r0","r1","r2","sp","r4",
"r5","r6","r7","r8","r9",
"r10","r11","r12","r13",
"r14","r15","r16","r17",
"r18","r19","r20","r21",
"r22","r23","r24","r25",
"r26","r27","r28","r29",
"r30","r31"};

const char *const v810_sysregisters[]=
{"EIPC","EIPSW","FEPC","FEPSW","ECR",
"PSW","PIR","TKCW","<Unk>","<Unk>",
"<Unk>","<Unk>","<Unk>","<Unk>","<Unk>",
"<Unk>","<Unk>","<Unk>","<Unk>","<Unk>",
"<Unk>","<Unk>","<Unk>","<Unk>","CHCW","ADTRE",
"<Unk>","<Unk>","<Unk>","<Unk>","<Unk>","<Unk>"
};

#define ERROR_BUFFER_SIZE 4 //max error logged

#define MODE_CPU	0
#define	MODE_PCFX	1
#define MODE_VBOY	2




#define word  	signed long     //32bit
#define half 	signed short    //16bit
#define byte  	signed char	  	//8bit

//format 1, 2
#define getReg2(opcode)		v810_registers[((opcode)>>5)&0x1f]
#define getReg1(opcode) 	v810_registers[(opcode)&0x1f]

//format 2
#define getImm5(opcode) 	((opcode)&0x1f)
#define getCondF2(opcode) 	((opcode)&0x1f)
#define getRegID(opcode) 	v810_sysregisters[(opcode)&0x1f] //for ldsr / stsr
#define getVector(opcode) 	((opcode)&0x1f)
#define getSubCode(opcode) 	((opcode)&0x1f)

byte getSignedImm5(half opcode){
	byte imm5 = opcode&0x1f;
	if (opcode&0x10){
		//negative
		imm5 |= 0xfe;
	}
	
	return imm5;
}	

//format 3
half getDisp9(half opcode){
	half disp9 = opcode&0x1ff;
	if (disp9&0x100){
		//negative
		disp9 |= 0xfe00;
	}
	
	return disp9;
}	

//format 4
word getDisp26(half opcode, half opcode2){
	word disp26 = opcode&0x3ff;
	disp26 <<= 16;
	disp26 |= opcode2;
	if (opcode&0x200){
		//negative
		disp26 |= 0xfc000000;
	}
	
	return disp26;
}

//format 5
#define getImm16(opcode)		(opcode)
#define getSignedImm16(opcode) 	((half) (opcode))

//format 6
#define getDisp16(opcode)		getSignedImm16(opcode)


struct arg_lit *pcfxFlag, *vbFlag, *showPCFlag, *showOpCodeFlag; 

void printOpCode(FILE *f, int low, int hi){
	//fprintf(f, "%02x%02x", low, hi);	
	if (showOpCodeFlag->count)	fprintf(f, "%02x%02x", hi, low);	
}

static void printver()
{
	printf("V810 disassembler v0.1\n");
	printf("KanedaFr 2021\n");
	printf("based on Virtual Boy - Sacred Tech Scroll and trap0xF's PCFX document\n");
	printf("Thanks to Tomasz Slanina and David Tucker for disassemblers, used to fix mine\n");
}

int main(int argc, char *argv[]) {
	struct arg_lit *help, *version;
	struct arg_file *file, *output;
	struct arg_int	*size_option;
	struct arg_dbl	*mapping_option;
	struct arg_int	*start_option;
	struct arg_end *end;

	int exitcode = 0;
	int err;


	unsigned char lowB, highB, lowB2, highB2;	     // up to 4 bytes for instruction (either 16 or 32 bits)
	unsigned short opcode;
	int     pc = 0;
	int     initAddress = 0;
	int     maxBytes = 0;
	int		mappedAddress = 0xFFF00000;
	int		mode = 0;

	FILE    *infile = NULL, *outfile = NULL;


	void *argtable[] = {
        version = arg_litn(NULL, "version", 0, 1, "display version info"),
        help    = arg_litn(NULL, "help", 0, 1, "display this help"),
        file    = arg_file1("i", "input", "<infile>","input file"),
        output  = arg_file1("o", "output", "<outfile>","output file"),
		mapping_option  = arg_dbl0(NULL, "map", "<value>","simulate mapping at address (default 0x00000000)"),
		start_option 	= arg_int0(NULL, "start", "<value>","addresse to start disassembly (default: 0)"),
		size_option   	= arg_int0(NULL, "max", "<value>","max bytes to disassemble (default 0 = full)"),
		pcfxFlag= arg_litn("x", "pcfx", 0, 1, "add PC-FX comments" ),
		vbFlag	= arg_litn("v", "vboy", 0, 1, "support VirtualBoy dedicated instruction set" ),
		showPCFlag	= arg_litn("p", "showPC", 0, 1, "show PC address" ),
		showOpCodeFlag	= arg_litn("d", "showData", 0, 1, "show data interpreted as OPCode" ),
        end     = arg_end(ERROR_BUFFER_SIZE),
    };

	if (arg_nullcheck(argtable) != 0)
	{
		printf("error: insufficient memory\n");
		exitcode = -1;
		goto exit;
    }

	//default values
    size_option->ival[0]=0;
    start_option->ival[0]=0;
    mapping_option->dval[0]=0; //0xFFF00000 for bios

	//get values
    err = arg_parse(argc,argv,argtable);

	 // special case 1: '--help' takes precedence over error reporting
    if (help->count > 0)
    {
		printver();
        printf("Usage:\txfcp");
        arg_print_syntax(stdout, argtable, "\n");
        arg_print_glossary(stdout, argtable, "  %-25s %s\n");
		exitcode = 0;
		goto exit;
    }

	// special case 2
	if (version->count)
	{
		printver();
		exitcode = 0;
		goto exit;
	}
	
	// If the parser returned any errors then display them and exit
    if ((err > 0) || (argc == 1))
    {
        // print upto ERROR_BUFFER_SIZE error details contained in the arg_end struct
        arg_print_errors(stdout, end, "xfcp");
        printf("Try '--help' for more information.\n");
		exitcode = -1;
		goto exit;
    }

	if (vbFlag->count && pcfxFlag->count){
		printf("Error : PC-FX or VirtualBoy, not the 2 of them at the same time\n");
		exitcode = -1;
		goto exit;
	}

	mode = MODE_CPU;
	if (vbFlag->count)	mode = MODE_VBOY;
	else if (pcfxFlag->count)	mode = MODE_PCFX;

	mappedAddress = (long) mapping_option->dval[0];
	initAddress = start_option->ival[0];
	maxBytes = size_option->ival[0];
	

	pc = initAddress;

	// Open input file.
	if((infile = fopen(file->filename[0], "rb")) == NULL) {
		printf("\nCannot open input file %s.", file->filename[0]);
		exit(1);
	}

	// Open output file.
	if((outfile = fopen(output->filename[0], "w")) == NULL) {
		printf("\nCannot open output file %s.", output->filename[0]);
		exit(1);
	}

	while(!feof(infile)) {
		if ((maxBytes != 0) && (pc >= maxBytes+initAddress))	break;

		fseek(infile, pc, SEEK_SET);
		
		lowB = getc(infile);
		highB = getc(infile);
		lowB2 = getc(infile);
		highB2 = getc(infile);

		fprintf(outfile, "\n");

		if (showPCFlag->count)
			fprintf(outfile, "%08x\t",mappedAddress + pc);
		printOpCode(outfile, lowB, highB);
		
		

		/*
		if (logicClean)
		{
			if(highB == 0xFF) 
				if(lowB == 0xFF) {
				pc += 2;						
				continue;
			}
			if(highB == 0x00) 
				if(lowB== 0x00) {
				pc += 2;						
				continue;
				}
		}
		*/
		

		unsigned short opc = highB;
		opc <<= 8;
		opc |= lowB;

		unsigned short opc2 = highB2;
		opc2 <<= 8;
		opc2 |= lowB2;

		opcode = highB >>2; 
		switch(opcode)
		{
			//format 1
			case 0x00: //Move register
				fprintf(outfile, "\tmov\t%s,%s",getReg1(opc),getReg2(opc));
				break;
			case 0x01: //Add register
				fprintf(outfile, "\tadd\t%s,%s",getReg1(opc),getReg2(opc));
				break;
			case 0x02: //Substract
				fprintf(outfile, "\tsub\t%s,%s",getReg1(opc),getReg2(opc));
				break;
			case 0x03: //Compare register
				fprintf(outfile, "\tcmp\t%s,%s",getReg1(opc),getReg2(opc));
				break;
			case 0x04: //Shift Logical Left by Register
				fprintf(outfile, "\tshl\t%s,%s",getReg1(opc),getReg2(opc));
				break;
			case 0x05: //Shift Logical Right by Register
				fprintf(outfile, "\tshr\t%s,%s",getReg1(opc),getReg2(opc)); 
				break;
			case 0x06: //Jump Register
				fprintf(outfile, "\tjmp\t[%s]",getReg1(opc));
				break;
			case 0x07: //Shift Arithmetic Right by Register
				fprintf(outfile, "\tsar\t%s,%s",getReg1(opc),getReg2(opc));
				break;
			case 0x08: //Multiply
				fprintf(outfile, "\tmul\t%s,%s",getReg1(opc),getReg2(opc));
				break;
			case 0x09: //Divide
				fprintf(outfile, "\tdiv\t%s,%s",getReg1(opc),getReg2(opc));
				break;
			case 0x0a: //Multiply Unsigned
				fprintf(outfile, "\tmulu\t%s,%s",getReg1(opc),getReg2(opc));
				break;
			case 0x0b: //Divide Unsigned
				fprintf(outfile, "\tdivu\t%s,%s",getReg1(opc),getReg2(opc));
				break;
			case 0x0c: //OR
				fprintf(outfile, "\tor\t%s,%s",getReg1(opc),getReg2(opc));
				break;
			case 0x0d: //AND
				fprintf(outfile, "\tand\t%s,%s",getReg1(opc),getReg2(opc)); 
				break;
			case 0x0e: //XOR
				fprintf(outfile, "\txor\t%s,%s",getReg1(opc),getReg2(opc));
				break;
			case 0x0f: //NOT
				fprintf(outfile, "\tnot\t%s,%s",getReg1(opc),getReg2(opc));
				break;

			//format 2
			case 0x10: //Move Immediate
				fprintf(outfile, "\tmov\t%d,%s",getSignedImm5(opc),getReg2(opc));
				break;
			case 0x11: //Add Immediate
				fprintf(outfile, "\tadd\t%d,%s",getSignedImm5(opc),getReg2(opc));
				break;
			case 0x12: //Set Flag Condition
				fprintf(outfile, "\tsetf\t0x%X,%s",getImm5(opc),getReg2(opc));
				break;
			case 0x13: //Compare Immediate
				fprintf(outfile, "\tcmp\t%d,%s",getSignedImm5(opc),getReg2(opc));
				break;
			case 0x14: //Shift Logical Left by Immediate
				fprintf(outfile, "\tshl\t%d,%s",getImm5(opc),getReg2(opc));
				break;
			case 0x15: //Shift Logical Right by Immediate
				fprintf(outfile, "\tshr\t%d,%s",getImm5(opc),getReg2(opc)); 
				break;
			case 0x16: //Clear Interrupt Disable Flag
				fprintf(outfile, "\tcli");
				break;
			case 0x17: //Shift Arithmetic Right by Immediate
				fprintf(outfile, "\tsar\t%d,%s",getImm5(opc),getReg2(opc));
				break;
			case 0x18: //Trap
				fprintf(outfile, "\ttrap\t0x%X",getImm5(opc)); 					
				break;
			case 0x19: //Return from Trap or Interrupt
				fprintf(outfile, "\treti"); 								
				break;
			case 0x1a: //Halt
				fprintf(outfile, "\thalt"); 								
				break;
			//case 0x1b:	break;
			case 0x1c: //Load to System Register
				fprintf(outfile, "\tldsr\t%s,%s",getReg2(opc),getRegID(opc));
				break;
			case 0x1d: //Store Contents of System Register
				fprintf(outfile, "\tstsr\t%s,%s",getRegID(opc),getReg2(opc));
				break;
			case 0x1e: //Set Interrupt Disable Flag
				fprintf(outfile, "\tsei");
				break;
			case 0x1f: //BitString
					switch(opc&0x1f)
					{
						case 0x00: //Search Bit 0 Upward
							fprintf(outfile, "\tSCH0BSU");
							break;
						case 0x01: //Search Bit 0 Downward
							fprintf(outfile, "\tSCH0BSD");
							break;
						case 0x02: //Search Bit 1 Upward
							fprintf(outfile, "\tSCH1BSU");
							break;
						case 0x03: //Search Bit 1 Downward
							fprintf(outfile, "\tSCH1BSD");
							break;
						//case 0x04:  break;
						//case 0x05:  break;
						//case 0x06:  break;
						//Case 0x07:  break;
						case 0x08: //Or Bit String Upward
							fprintf(outfile, "\tORBSU");
							break;
						case 0x09: //And Bit String Upward
							fprintf(outfile, "\tANDBSU");
							break;
						case 0x0a: //Exclusive Or Bit String Upward
							fprintf(outfile, "\tXORBSU");
							break;
						case 0x0b: //Move Bit String Upward
							fprintf(outfile, "\tMOVBSU");
							break;
						case 0x0c: //Or Not Bit String Upward
							fprintf(outfile, "\tORNBSU");
							break;
						case 0x0d: //And Not Bit String Upward
							fprintf(outfile, "\tANDNBSU");
							break;
						case 0x0e: //Exclusive Or Not Bit String Upward
							fprintf(outfile, "\tXORNBSU");
							break;
						case 0x0f: //NOT Bit String Upward
							fprintf(outfile, "\tNOTBSU");
							break;

						//invalid
						case 0x04:
						case 0x05:
						case 0x06:
						case 0x07:
						//case 0x10+
						default:
							fprintf(outfile, "\t ;Invalid Opcode");
							break;
					}
					break;

			//Format 3 -- BCond
			case 0x20:
			case 0x21:
			case 0x22:
			case 0x23:
			case 0x24:
			case 0x25:
			case 0x26:
			case 0x27: 
					switch( (opc>>9) &0xf)
					{
						case 0x0: fprintf(outfile, "\tbv\t%d\t\t; so 0x%X",getDisp9(opc), mappedAddress + pc+getDisp9(opc));  break;
						case 0x1: fprintf(outfile, "\tbl\t%d\t\t; so 0x%X",getDisp9(opc), mappedAddress + pc+getDisp9(opc));  break;
						case 0x2: fprintf(outfile, "\tbe\t%d\t\t; so 0x%X",getDisp9(opc), mappedAddress + pc+getDisp9(opc));  break;
						case 0x3: fprintf(outfile, "\tbnh\t%d\t\t; so 0x%X",getDisp9(opc), mappedAddress + pc+getDisp9(opc)); break;
						case 0x4: fprintf(outfile, "\tbn\t%d\t\t; so 0x%X",getDisp9(opc), mappedAddress + pc+getDisp9(opc));  break;
						case 0x5: fprintf(outfile, "\tbr\t%d\t\t; so 0x%X",getDisp9(opc), mappedAddress + pc+getDisp9(opc));  break;
						case 0x6: fprintf(outfile, "\tblt\t%d\t\t; so 0x%X",getDisp9(opc), mappedAddress + pc+getDisp9(opc)); break;
						case 0x7: fprintf(outfile, "\tble\t%d\t\t; so 0x%X",getDisp9(opc), mappedAddress + pc+getDisp9(opc)); break;
						case 0x8: fprintf(outfile, "\tbnv\t%d\t\t; so 0x%X",getDisp9(opc), mappedAddress + pc+getDisp9(opc)); break;
						case 0x9: fprintf(outfile, "\tbnl\t%d\t\t; so 0x%X",getDisp9(opc), mappedAddress + pc+getDisp9(opc)); break;
						case 0xa: fprintf(outfile, "\tbne\t%d\t\t; so 0x%X",getDisp9(opc), mappedAddress + pc+getDisp9(opc)); break;
						case 0xb: fprintf(outfile, "\tbh\t%d\t\t; so 0x%X",getDisp9(opc), mappedAddress + pc+getDisp9(opc));  break;
						case 0xc: fprintf(outfile, "\tbp\t%d\t\t; so 0x%X",getDisp9(opc), mappedAddress + pc+getDisp9(opc));  break;
						case 0xd: fprintf(outfile, "\tnop"); break;
						case 0xe: fprintf(outfile, "\tbge\t%d\t\t; so 0x%X",getDisp9(opc), mappedAddress + pc+getDisp9(opc)); break;
						case 0xf: fprintf(outfile, "\tbgt\t%d\t\t; so 0x%X",getDisp9(opc), mappedAddress + pc+getDisp9(opc)); break;
					}
					break;

			case 0x28:  //MoveAdd
					printOpCode(outfile, lowB2, highB2);
					fprintf(outfile, "\tmovea\t%d, %s, %s",getSignedImm16(opc2),getReg1(opc),getReg2(opc));
					pc += 2;
					break;
			case 0x29: //Add Immediate
					printOpCode(outfile, lowB2, highB2);
					fprintf(outfile, "\taddi\t%d, %s, %s",getSignedImm16(opc2),getReg1(opc),getReg2(opc));
					pc += 2;
					break;
			case 0x2a:  //Jump Relative
					printOpCode(outfile, lowB2, highB2);
					fprintf(outfile, "\tjr\t%ld \t\t; so 0x%lX", getDisp26(opc,opc2), (0xFFFFFFFF & (mappedAddress + pc+getDisp26(opc,opc2))));
					pc += 2; 
					break;
			case 0x2b:  //Jump and Link
					printOpCode(outfile, lowB2, highB2);
					fprintf(outfile, "\tjal\t%ld \t\t; so 0x%lX", getDisp26(opc,opc2), (0xFFFFFFFF & (mappedAddress + pc+getDisp26(opc,opc2))));
					pc += 2; 
					break;
			case 0x2c:  //Or Immediate
					printOpCode(outfile, lowB2, highB2);
					fprintf(outfile, "\tori\t0x%X, %s, %s",getImm16(opc2),getReg1(opc),getReg2(opc));
					pc += 2; 
					break;
			case 0x2d:  //And Immediate
					printOpCode(outfile, lowB2, highB2);
					fprintf(outfile, "\tandi\t0x%X, %s, %s",getImm16(opc2),getReg1(opc),getReg2(opc));
					pc += 2; 
					break;
			case 0x2e:  //XOR Immediate
					printOpCode(outfile, lowB2, highB2);
					fprintf(outfile, "\txori\t0x%X, %s, %s",getImm16(opc2),getReg1(opc),getReg2(opc));
					pc += 2; 
					break;
			case 0x2f:  //Add
					printOpCode(outfile, lowB2, highB2);
					fprintf(outfile, "\tmovhi\t0x%X, %s, %s",getImm16(opc2),getReg1(opc),getReg2(opc));
					pc += 2; 
					break;
			case 0x30:   //Load Byte
					printOpCode(outfile, lowB2, highB2);
					fprintf(outfile, "\tld.b\t0x%X[%s], %s",getDisp16(opc2),getReg1(opc),getReg2(opc));
					pc += 2; 
					break;
			case 0x31:   //Load Half
					printOpCode(outfile, lowB2, highB2);
					fprintf(outfile, "\tld.h\t0x%X[%s], %s",getDisp16(opc2),getReg1(opc),getReg2(opc));
					pc += 2;
					break;
			//case 0x32:  break;
			case 0x33:  //Load Word
					printOpCode(outfile, lowB2, highB2);
					fprintf(outfile, "\tld.w\t0x%X[%s], %s",getDisp16(opc2),getReg1(opc),getReg2(opc));
					pc += 2; 
					break;
			case 0x34:  //Store Byte
					printOpCode(outfile, lowB2, highB2);
					fprintf(outfile, "\tst.b\t%s, 0x%X[%s]",getReg2(opc),getDisp16(opc2),getReg1(opc));
					pc += 2; 
					break;
			case 0x35:  //Store Half
					printOpCode(outfile, lowB2, highB2);
					fprintf(outfile, "\tst.h\t%s, 0x%X[%s]",getReg2(opc),getDisp16(opc2),getReg1(opc));
					pc += 2; 
					break;
			//case 0x36:  break;
			case 0x37:  //Store Word
					printOpCode(outfile, lowB2, highB2);
					fprintf(outfile, "\tst.w\t%s, 0x%X[%s]",getReg2(opc),getDisp16(opc2),getReg1(opc));
					pc += 2;
					break;
			case 0x38:  //Input Byte from Port
					printOpCode(outfile, lowB2, highB2);
					fprintf(outfile, "\tin.b\t0x%X[%s], %s",getDisp16(opc2),getReg1(opc),getReg2(opc));
					pc += 2;
					break;
			case 0x39:  ////Input Byte from Half
					printOpCode(outfile, lowB2, highB2);
					fprintf(outfile, "\tin.h\t0x%X[%s], %s",getDisp16(opc2),getReg1(opc),getReg2(opc));
					pc += 2; 
					break;
			case 0x3a:  //Compare and Exchange Interlocked
					printOpCode(outfile, lowB2, highB2);
					fprintf(outfile, "\tcaxi\t0x%X[%s], %s",getDisp16(opc2),getReg1(opc),getReg2(opc));
					pc += 2; 
					break;
			case 0x3b:  //Input Word from Port
					printOpCode(outfile, lowB2, highB2);
					fprintf(outfile, "\tin.w\t0x%X[%s], %s",getDisp16(opc2),getReg1(opc),getReg2(opc));
					pc += 2; 
					break;
			case 0x3c:  //Output Byte to Port
					printOpCode(outfile, lowB2, highB2);
					fprintf(outfile, "\tout.b\t%s, 0x%X[%s]",getReg2(opc),getDisp16(opc2),getReg1(opc));
					pc += 2; 
					break;
			case 0x3d:  //Output Half to Port
					printOpCode(outfile, lowB2, highB2);
					fprintf(outfile, "\tout.h\t%s, 0x%X[%s]",getReg2(opc),getDisp16(opc2),getReg1(opc));
					pc += 2;
					break;
			case 0x3e: //Floating point
					printOpCode(outfile, lowB2, highB2);
					switch((opc2&0xfc00)>>10)
					{
						case 0x0: //Compare Floating Short
							fprintf(outfile, "\tcmpf.s\t%s, %s",getReg1(opc),getReg2(opc)); 
							break;
						//case 0x1: break;
						case 0x2: //Convert Word Integer to Short Floating
							fprintf(outfile, "\tcvt.ws\t%s, %s",getReg1(opc),getReg2(opc)); 
							break;
						case 0x3: //Convert Short Floating to Word Integer
							fprintf(outfile, "\tcvt.sw\t%s, %s",getReg1(opc),getReg2(opc)); 
							break;
						case 0x4: //Add Floating Short
							fprintf(outfile, "\taddf.s\t%s, %s",getReg1(opc),getReg2(opc)); 
							break;
						case 0x5: //Subtract Floating Short
							fprintf(outfile, "\tsubf.s\t%s, %s",getReg1(opc),getReg2(opc)); 
							break;
						case 0x6: //Multiply Floating Short
							fprintf(outfile, "\tmulf.s\t%s, %s",getReg1(opc),getReg2(opc)); 
							break;
						case 0x7: //Divide Floating Short
							fprintf(outfile, "\tdivf.s\t%s, %s",getReg1(opc),getReg2(opc));
							break;
						case 0xb: //Truncate Short Floating to Word Integer
							fprintf(outfile, "\ttrnc.sw\t%s, %s",getReg1(opc),getReg2(opc));
							break;
						
						//nintendo on VB
						case 0x8:  //Exchange Byte
							if (mode == MODE_VBOY){
								fprintf(outfile, "\txb\t%s, %s",getReg1(opc),getReg2(opc));
								break;
							}
						case 0x9: //Exchange Halfword
							if (mode == MODE_VBOY){
								fprintf(outfile, "\txh\t%s, %s",getReg1(opc),getReg2(opc));
								break;
							}
						case 0xa: //Reverse Bits in Word
							if (mode == MODE_VBOY){
								fprintf(outfile, "\trev\t%s, %s",getReg1(opc),getReg2(opc));
								break;
							}
						case 0xc: //Multiply Halfword
							if (mode == MODE_VBOY){
								fprintf(outfile, "\tmpyhw\t%s, %s",getReg1(opc),getReg2(opc));
								break;
							}

						//invalid
						case 0x1:
						case 0xd:
						case 0xe:
						case 0xf:
						default : 
							fprintf(outfile, "\t ;Invalid Opcode");
							//fprintf(outfile, "\tUnkf 0x%X",(opc2&0xfc00)>>10);
							break;
					}
					pc += 2;
					break;
			case 0x3f:  //Output Word to Port
				printOpCode(outfile, lowB2, highB2);
				fprintf(outfile, "\tout.w\t%s, 0x%X[%s]",getReg2(opc),getImm16(opc2),getReg1(opc));
				pc += 2;
				break;

			case 0x1b:
			case 0x32:
			case 0x36:
			//case 0x40+
			default:
				fprintf(outfile, "\t ;Invalid Opcode");
		}
		
		pc += 2;
	}

exit:
	if (infile != NULL)		fclose(infile);	
	if (outfile != NULL)	fclose(outfile);

	arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
    return exitcode;
}


